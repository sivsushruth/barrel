-define(default_fold_options,
  #{
    start_key => first,
    end_key => nil,
    gt => nil,
    gte => nil,
    lt => nil,
    lte => nil,
    max => 0,
    move => next
  }
).