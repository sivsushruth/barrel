# Unit tests for the HTTP API

Expect to have [jq](https://stedolan.github.io/jq) available.

1) Start a barrel server.

2) Execute the tests:

    $ ./shebang-unit run_test.sh
